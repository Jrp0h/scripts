#!/usr/bin/bash

on=`amixer -D pulse sget Master | grep 'Right:' | awk -F'[][]' '{ print $4 }'`
volume=`amixer -D pulse sget Master | grep 'Right:' | awk -F'[][]' '{ print $2 }'`

volume=${volume::-1}
if [ $on = "on" ] 
then
    echo $volume > /tmp/xobpipe
else
    echo "$volume!" > /tmp/xobpipe
fi

    