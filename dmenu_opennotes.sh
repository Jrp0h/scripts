#!/bin/bash

file="$(ls -r /home/marcus/Documents/notes/pdf | sed "s/ /\n/g" | dmenu -h 27 -p "Open file:")"

if [ $file ]; then
    fullPath="/home/marcus/Documents/notes/pdf/${file}"

    if [ -f $fullPath ]; then
        zathura $fullPath
    else
        notify-send -u critical "No note found" "No note for found or it's an invalid file!"
    fi
fi
