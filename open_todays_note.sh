#!/bin/bash

noteFilename="$HOME/Documents/notes/pdf/note-$(date +%Y-%m-%d).pdf"

if [ -f $noteFilename ]; then
    zathura $noteFilename
else
    notify-send -u critical "No note found" "No note for today found!"
fi
