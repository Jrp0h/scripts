#!/bin/sh

cat <<EOF | xmenu | sh &
Logout	bspc quit

Shutdown		poweroff
Reboot			reboot
EOF
